export interface IQuestionnaireEndpoint{
    getQuestionnaires(request,response);
    getQuestionnaire(request,response);
    getQuestions(request,response);
    getQuestion(request,response);
}