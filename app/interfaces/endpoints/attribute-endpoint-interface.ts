export interface IAttributeEndpoint{
    getAttributes(request,response);
    postAttributes(request,response);
}