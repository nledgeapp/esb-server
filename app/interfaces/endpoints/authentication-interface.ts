import * as Express from "express";

export interface IAuthentication{
    required(req: Express.Request,res: Express.Response,next: Express.NextFunction);
}