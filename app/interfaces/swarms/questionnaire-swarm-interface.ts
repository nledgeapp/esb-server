import { Observable } from "rxjs";
import { IQuestionnaire, IQuestion } from "../../entities/adapter/questionnaire-entities";
export interface IQuestionnaireSwarm{
    loadQuestionnaires():Observable<IQuestionnaire[]>;

    loadQuestionnaire(id: string):Observable<IQuestionnaire>;

    loadQuestions(id: string):Observable<IQuestion[]>;

    loadQuestion(id: string):Observable<IQuestion>;
}