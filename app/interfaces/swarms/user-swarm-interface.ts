import { Observable } from "rxjs";
import { ILinkedInProfile, ISlackProfile } from "../../entities/adapter/user-entities";
export interface IUserSwarm{
    getLinkedInProfile(linkedInToken:string):Observable<ILinkedInProfile>;
    getSlackProfile(slackId:string, slackToken:string):Observable<ISlackProfile>;
}