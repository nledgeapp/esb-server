import { Observable } from "rxjs";
import { IAttribute } from "../../entities/adapter/attribute-entities";
export interface IAttributeSwarm{
    getAttributes(params):Observable<IAttribute[]>;
    postAttribute(attribute:IAttribute):Observable<IAttribute>;
}