import { Observable } from "rxjs";
import { IQuestionnaire, IQuestion } from "../../entities/adapter/questionnaire-entities";

export interface IQuestionnaireAdapter{
    getQuestionnaires():Observable<IQuestionnaire[]>;
    getQuestionnaire(questionnaire_id: string):Observable<IQuestionnaire>;
    getQuestions(questionnaire_id: string):Observable<IQuestion[]>;
    getQuestion(id: string):Observable<IQuestion>;
}