import { Observable } from "rxjs";
import * as http from "http";

export interface IHttpJSONAdapter{

    getJSON<T>(useHttps:boolean, options:http.RequestOptions):Observable<T>;

    putJSON(useHttps:boolean, putData:any, options:http.RequestOptions):Observable<string>;

    postJSON<T>(useHttps:boolean, postData:any, options:http.RequestOptions):Observable<T>;

    deleteJSON(useHttps:boolean, options:http.RequestOptions):Observable<string>;
}