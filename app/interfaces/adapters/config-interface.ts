import * as http from "http";
export interface IAdapterConfig{
    adapter:{
        questionnaire:IAdapterEndpoint;
        attribute:IAdapterEndpoint;
        user:IAdapterEndpoint;
    }
}

export interface IAdapterEndpoint{
    options: http.RequestOptions,
    https:boolean;
}