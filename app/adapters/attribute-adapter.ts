import { IAttributeAdapter } from "../interfaces/adapters/attribute-adapter-interface";
import { inject, injectable } from "inversify";
import { ILogger } from "../logger/logger-interface";
import { IAttribute} from "../entities/adapter/attribute-entities";
import { IAdapterConfig } from "../interfaces/adapters/config-interface";
import { IHttpJSONAdapter } from "../interfaces/adapters/http-json-adapter-interface";
import { Observable } from "rxjs";
import { AbstractAdapter } from "./abstract-adapter";

@injectable()
export class AttributeAdapter extends AbstractAdapter implements IAttributeAdapter{

    constructor(
        @inject("Logger") private logger: ILogger,
        @inject("AdapterConfig") private config: IAdapterConfig,
        @inject("HttpJSONAdapter") httpJsonAdapter: IHttpJSONAdapter
    ) {
        super(config.adapter.attribute,httpJsonAdapter);
    }

    public getAttributes(params):Observable<IAttribute[]>{
        this.logger.debug("AttributeAdapter:getAttributes",params);
        let url = "/api";
        let queryParams: string[] = [];
        if(params){
            if(params.id){
                queryParams.push("id="+params.id);
            }
            if(params.name){
                queryParams.push("name="+params.name);
            }
            if(params.type){
                queryParams.push("type="+params.type);
            }
            if(params.source){
                queryParams.push("source="+params.source);
            }
            if(params.group){
                queryParams.push("group="+params.group);
            }
            if(queryParams.length > 0){
                url = url + "?" + queryParams.join("&");
            }
        }
        return this.getFromBackend<IAttribute>(url).toArray();
    }

    public postAttribute(attribute:IAttribute):Observable<IAttribute>{
        this.logger.debug("AttributeAdapter:postAttribute",attribute);
        return this.postToBackend<IAttribute>("/api",attribute);
    }
}