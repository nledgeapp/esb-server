import { Observable } from "rxjs";  
import { IAdapterEndpoint } from "../interfaces/adapters/config-interface";
import { IHttpJSONAdapter } from "../interfaces/adapters/http-json-adapter-interface";
import { inject, injectable } from "inversify";
@injectable()
export class AbstractAdapter{ 

    constructor (private adapterConfig: IAdapterEndpoint, private httpJsonAdapter: IHttpJSONAdapter)
    {
    }
    
    public getFromBackend<T>(path: string):Observable<T>{
        let options = Object.assign({},this.adapterConfig.options,{path: path});
        return this.httpJsonAdapter.getJSON<T>(this.adapterConfig.https, options);
    }

    public postToBackend<T>(path: string, object:T):Observable<T>{
        let options = Object.assign({},this.adapterConfig.options,{path: path});
        return this.httpJsonAdapter.postJSON<T>(this.adapterConfig.https, object, options);
    }
}