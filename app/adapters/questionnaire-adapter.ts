import { IQuestionnaireAdapter } from "../interfaces/adapters/questionnaire-adapter-interface";
import { inject, injectable } from "inversify";
import { ILogger } from "../logger/logger-interface";
import { IQuestionnaire, IQuestion } from "../entities/adapter/questionnaire-entities";
import { IAdapterConfig } from "../interfaces/adapters/config-interface";
import { IHttpJSONAdapter } from "../interfaces/adapters/http-json-adapter-interface";
import { Observable } from "rxjs";
import { AbstractAdapter } from "./abstract-adapter";
@injectable()
export class QuestionnaireAdapter extends AbstractAdapter implements IQuestionnaireAdapter {

    constructor(
        @inject("Logger") private logger: ILogger,
        @inject("AdapterConfig") private config: IAdapterConfig,
        @inject("HttpJSONAdapter") httpJsonAdapter: IHttpJSONAdapter
        ) {
            super(config.adapter.questionnaire, httpJsonAdapter);
        }

    public getQuestionnaires():Observable<IQuestionnaire[]>{
        this.logger.debug("QuestionnaireAdapter:getQuestionnaires");
        return this.getFromBackend<IQuestionnaire>("/api/questionnaire").toArray();
    }

    public getQuestionnaire(id): Observable<IQuestionnaire>{
        this.logger.debug("QuestionnaireAdapter:getQuestionnaire");      
        return <Observable<IQuestionnaire>>this.getFromBackend<IQuestionnaire>("/api/questionnaire/"+id);
    }

    public getQuestions(questionnaire_id?:string):Observable<IQuestion[]>{
        this.logger.debug("QuestionnaireAdapter:getQuestions",questionnaire_id);
        let path = questionnaire_id?"/api/question?questionnaire_id="+questionnaire_id:"/api/question/";
        return this.getFromBackend<IQuestion>(path).toArray();
    }

    public getQuestion(id:string):Observable<IQuestion>{
        this.logger.debug("QuestionnaireAdapter:getQuestion",id);
        return this.getFromBackend<IQuestion>("/api/question/"+id);
    }

    /*postQuestion(question:IQuestion):boolean{
        return true;
    }

    putQuestions(question:IQuestion):boolean{
        return true;
    }

    deleteQuestions(question:IQuestion){
        return true;
    }*/

}