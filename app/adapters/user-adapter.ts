import { IUserAdapter } from "../interfaces/adapters/user-adapter-interface";
import { inject, injectable } from "inversify";
import { ILogger } from "../logger/logger-interface";
import { IAdapterConfig } from "../interfaces/adapters/config-interface";
import { IHttpJSONAdapter } from "../interfaces/adapters/http-json-adapter-interface";
import { Observable } from "rxjs";
import { IUserAuthenticationCookies, ILinkedInProfile, ISlackProfile } from"../entities/adapter/user-entities";
import { AbstractAdapter } from "./abstract-adapter";

@injectable()
export class UserAdapter extends AbstractAdapter implements IUserAdapter{

    constructor(
        @inject("Logger") private logger: ILogger,
        @inject("AdapterConfig") private config: IAdapterConfig,
        @inject("HttpJSONAdapter") httpJsonAdapter: IHttpJSONAdapter
        ) {
            super(config.adapter.user,httpJsonAdapter);
        }

    public getLinkedInProfile( linkedinToken: string):Observable<ILinkedInProfile>{
        this.logger.debug("UserAdapter:getLinkedInProfile");
        return this.getFromBackend<ILinkedInProfile>(`/api/users/skills/linkedin/${linkedinToken}`);
    }

    public getSlackProfile(slackId: string, slackToken: string):Observable<ISlackProfile>{
        this.logger.debug("UserAdapter:getSlackProfile",`/api/users/skills/slack?userId=${slackId}&accessToken=${slackToken}`);
        return this.getFromBackend<ISlackProfile>(`/api/users/skills/slack?userId=${slackId}&accessToken=${slackToken}`);
    }
}