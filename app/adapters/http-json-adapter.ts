import * as http from "http";
import * as https from "https";
import { Observable, Subject } from "rxjs";
import { inject, injectable } from "inversify";
import { ILogger } from "../logger/logger-interface";
import { IHttpJSONAdapter } from "../interfaces/adapters/http-json-adapter-interface";

@injectable()
export class HttpJSONAdapter implements IHttpJSONAdapter{

    constructor(@inject("Logger") private logger: ILogger){}

    getJSON<T>(useHttps:boolean, options:http.RequestOptions):Observable<T>{
        if(!options.headers){
            options.headers = {};
        }
        if(!options.method){
            options.method = "GET"
        }
        let response = new Subject<T>();
        let protocol:any = useHttps?https:http;
        protocol.get(options, (result) => this.receiveJSONResponse<T>(result,response));
        return response;
    }

    putJSON(useHttps:boolean, putData:any, options:http.RequestOptions):Observable<string>{
        let put_data_string = JSON.stringify(putData);
        if(!options.headers){
            options.headers = {};
        }
        if(!options.method){
            options.method = "PUT"
        }
        options.headers["Content-Type"]='application/x-www-form-urlencoded';
        options.headers["Content-Length"]= Buffer.byteLength(put_data_string);  
        let response = new Subject<string>();
        let protocol:any = useHttps?https:http;
        let request = protocol.request(options, (result) => this.receiveStatusResponse(result,response));
        request.write(put_data_string);
        request.end();
        return response;
    }

    postJSON(useHttps:boolean, postData:any, options:http.RequestOptions):Observable<JSON>{
        let post_data_string = JSON.stringify(postData);
        if(!options.headers){
            options.headers = {};
        }
        if(!options.method){
            options.method = "POST"
        }
        options.headers["Content-Type"]='application/x-www-form-urlencoded';
        options.headers["Content-Length"]= Buffer.byteLength(post_data_string);      
        let response = new Subject<JSON>();
        let protocol:any = useHttps?https:http;
        let request = protocol.request(options, (result) => this.receiveJSONResponse(result,response));
        request.write(post_data_string);
        request.end();
        return response;
    }

    deleteJSON(useHttps:boolean, options:http.RequestOptions):Observable<string>{
        if(!options.headers){
            options.headers = {};
        }
        if(!options.method){
            options.method = "DELETE"
        }
        let response = new Subject<string>();
        let protocol:any = useHttps?https:http;
        protocol.request(options, (result) => this.receiveStatusResponse(result,response));
        return response;
    }

    private receiveJSONResponse<T>(result, response: Subject<T>){
        var chunks = [];
        result.on("error", (error) => {
            response.error(error);
        });
        result.on("data", (chunk) => chunks.push(chunk));
        result.on("end", (x)=> {
            if( 200 <= result.statusCode && result.statusCode < 300 ){
                let json = JSON.parse(Buffer.concat(chunks).toString());
                if(Array.isArray(json)){
                    Observable.from(json).subscribe(response);
                }
                else{
                    response.next(json);
                }
            }
            else{
                this.receiveErrorResponse(result, response);
            }
        });
    }

    private receiveStatusResponse(result, response: Subject<string>){
        if(result.StatusCode <300 && result.StatusCode >= 200){
            response.next(result.StatusCode);
        }
        else{
            this.receiveErrorResponse(result, response);
        }
    }

    private receiveErrorResponse(result, response: Subject<any>){
        let code = result.statusCode.toString();
        let message = "";
        switch(result.statusCode){
            case 500:{
                message = "Internal Server Error";
                break;
            }
            case 501:{
                message = "Not Implemented";
                break;
            }
            case 502:{
                message = "Bad Gateway";
                break;
            }
            case 503:{
                message = "Service unavailable";
                break;
            }
            case 504:{
                message = "Gateway Time-out";
                break;
            }
            case 505:{
                message = "HTTP Version not supported";
                break;
            }
            case 506:{
                message = "Variant Also Negotiates";
                break;
            }
            case 507:{
                message = "Insufficient Storage";
                break;
            }
            case 508:{
                message = "Loop Detected";
                break;
            }
            case 509:{
                message = "Bandwidth Limit Exceeded";
                break;
            }
            case 510:{
                message = "Not Extended";
                break;
            }
            case 511:{
                message = "Network Authentication Required";
                break;
            }
            default:{
                message = "Unknown error";
                break;
            }                    
        }
        response.error(message+' - '+code);
    }
}


