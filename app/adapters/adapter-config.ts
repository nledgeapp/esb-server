import { IAdapterConfig } from "../interfaces/adapters/config-interface";
import { injectable } from "inversify";

@injectable()
export class AdapterConfig implements IAdapterConfig{
    adapter = {
        questionnaire:{
            options: {
                hostname:"edge-summit-questionnaire-srv.herokuapp.com",
                port:443,
            },
            https:true
        },
        attribute:{
            options: {
                hostname:"edge-summit-attribute-service.herokuapp.com",
                port:443
            },
            https:true
        },
        user:{
            options:{
                hostname:"edge-summit-user-service.herokuapp.com",
                port:443
            },
            https:true
        }
    }
}