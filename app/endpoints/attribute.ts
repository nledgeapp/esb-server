import { IAttributeSwarm } from "../interfaces/swarms/attribute-swarm-interface";
import { IAttributeEndpoint } from "../interfaces/endpoints/attribute-endpoint-interface";
import { inject, injectable } from "inversify";
import { ILogger } from "../logger/logger-interface";
import { Observable } from "rxjs";
import { IAttribute } from "../entities/adapter/attribute-entities";
import * as express from 'express';
@injectable()
export class AttributeEndpoint implements IAttributeEndpoint{

    constructor(
            @inject("AttributeSwarm") private attributeSwarm: IAttributeSwarm,
            @inject("Logger") private logger: ILogger
    ){}

    getAttributes(req: express.Request, res: express.Response){
        this.logger.debug("AttributeEndpoint:getAttributes");
        let queryParams = req.query;
        this.attributeSwarm.getAttributes(queryParams).subscribe((attributes:IAttribute[]) => {
            this.logger.debug("AttributeEndpoint:getAttributes returns:",attributes);
            res.setHeader("content-type","application/json");
            res.send(JSON.stringify(attributes));
        });
    }

    postAttributes(req: express.Request, res: express.Response){
        this.logger.debug("AttributeEndpoint:postAttributes");
        let attribute = <IAttribute>req.body;
        this.attributeSwarm.postAttribute(attribute).subscribe((_attribute:IAttribute) => {
            this.logger.debug("AttributeEndpoint:postAttributes returns:",_attribute);
            res.setHeader("content-type","application/json");
            res.send(JSON.stringify(_attribute));
        });
    }
}