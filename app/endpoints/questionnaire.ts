import { IQuestionnaireSwarm } from "../interfaces/swarms/questionnaire-swarm-interface";
import { IQuestionnaireEndpoint } from "../interfaces/endpoints/questionnaire-endpoint-interface";
import { inject, injectable } from "inversify";
import { ILogger } from "../logger/logger-interface";
import { Observable } from "rxjs";
import { IQuestionnaire, IQuestion } from "../entities/adapter/questionnaire-entities";
import * as express from 'express';

@injectable()
export class QuestionnaireEndpoint implements IQuestionnaireEndpoint{

    constructor(
            @inject("QuestionnaireSwarm") private questionnaireSwarm: IQuestionnaireSwarm,
            @inject("Logger") private logger: ILogger
    ){}

    getQuestionnaires(req: any, res: any){
        this.logger.debug("QuestionnaireEndpoint:getQuestionnaires");
        this.questionnaireSwarm.loadQuestionnaires().subscribe((questionnaires:IQuestionnaire[]) => {
            this.logger.debug("QuestionnaireEndpoint:getQuestionnaires returns:",questionnaires);
            res.setHeader("content-type","application/json");
            res.send(JSON.stringify(questionnaires));
        });
    }

    getQuestionnaire(req: any, res: any){
        this.logger.debug("QuestionnaireEndpoint:getQuestionnaire");
        let id: string = req.params.id;
        this.questionnaireSwarm.loadQuestionnaire(id).subscribe((questionnaire:IQuestionnaire) => {
            this.logger.debug("QuestionnaireEndpoint:getQuestionnaire returns:",questionnaire);
            res.setHeader("content-type","application/json");
            res.send(JSON.stringify(questionnaire));
        });
    }

    getQuestions(req: any, res: any){
        this.logger.debug("QuestionnaireEndpoint:getQuestions");
        let id = req.params.id;
        let json = this.questionnaireSwarm.loadQuestions(id).subscribe(questions=>{
            this.logger.debug("QuestionnaireEndpoint:getQuestions returns:",questions);
            res.setHeader("content-type","application/json");
            res.send(questions);
        },
        ()=>{
            this.logger.error("Couldn't receive questions with id:",id);
            res.sendStatus(500);
        });
    }

    getQuestion(req: any, res: any){
        this.logger.debug("QuestionnaireEndpoint:getQuestion");
        let id = req.params.id;
        let json = this.questionnaireSwarm.loadQuestion(id).subscribe(question=>{
            this.logger.debug("QuestionnaireEndpoint:getQuestion returns:",question);
            res.setHeader("content-type","application/json");
            res.send(question);
        });
    }
}