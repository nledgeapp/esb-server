import { IUserSwarm } from "../interfaces/swarms/user-swarm-interface";
import { IUserAuthenticationCookies } from"../entities/adapter/user-entities";
import { inject, injectable } from "inversify";
import { ILogger } from "../logger/logger-interface";
import * as Express from "express";

@injectable()
export class Authentication {

    constructor(
            @inject("UserSwarm") private userSwarm: IUserSwarm,
            @inject("Logger") private logger: ILogger
    ){}

    public required(req: Express.Request,res: Express.Response,next: Express.NextFunction){
        this.logger.debug("Authentication:required");
        let cookies: IUserAuthenticationCookies = req.cookies;
        this.logger.debug("Authentication:required cookies received", req.cookies);
        let errorHandling = () => {
            this.logger.debug("Authentication:required resulted in error");
            res.sendStatus(204);
        }
        let success = () => {
            this.logger.debug("Authentication:required resulted in success");
            next();
        }
        if(cookies && cookies.slackId && cookies.slackToken){
            this.logger.debug("Authentication:required trying slack Verification");
            this.userSwarm.getSlackProfile(cookies.slackId,cookies.slackToken).map((profile => !!profile)).subscribe(success,errorHandling);
        }
        else if(cookies && cookies.linkedinToken){
            this.logger.debug("Authentication:required trying linkedin Verification");
            this.userSwarm.getLinkedInProfile(cookies.linkedinToken).map((profile => !!profile)).subscribe(success,errorHandling);
        }
        else{
            errorHandling();
        }
    }
}