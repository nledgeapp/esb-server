import * as log from 'bog';
import { ILogger } from "./logger-interface";
import { injectable } from "inversify";

@injectable()
export class BogLogger {

    debug(...args:any[]){
        log.debug(...args);
    }

    info(...args:any[]){
        log.info(...args);
    }

    warn(...args:any[]){
        log.warn(...args);
    }

    error(...args:any[]){
        log.error(...args);
    }    
}