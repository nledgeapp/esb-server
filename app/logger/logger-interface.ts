export interface ILogger {
    debug(...args:any[]);  
    info(...args:any[]); 
    warn(...args:any[]);
    error(...args:any[]);  
}