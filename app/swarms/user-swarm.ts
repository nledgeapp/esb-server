import { inject, injectable } from "inversify";
import { IUserAdapter } from "../interfaces/adapters/user-adapter-interface";
import { IUserSwarm } from "../interfaces/swarms/user-swarm-interface";
import { Observable } from "rxjs";
import { ILinkedInProfile, ISlackProfile } from "../entities/adapter/user-entities";

import { ILogger } from "../logger/logger-interface";
@injectable()
export class UserSwarm implements IUserSwarm{

    constructor(
        @inject("UserAdapter") private userAdapter: IUserAdapter,
        @inject("Logger") private logger: ILogger) {}

    getLinkedInProfile(linkedInToken:string):Observable<ILinkedInProfile>{
        this.logger.debug("UserSwarm:getLinkedInProfile:",linkedInToken);
        return this.userAdapter.getLinkedInProfile(linkedInToken);
    }

    getSlackProfile(slackId:string, slackToken:string):Observable<ISlackProfile>{
        this.logger.debug("UserSwarm:getSlackProfile:",slackId,slackToken);
        return this.userAdapter.getSlackProfile(slackId, slackToken);
    }

}