import { IQuestionnaireAdapter } from "../interfaces/adapters/questionnaire-adapter-interface";
import { IQuestionnaireSwarm } from "../interfaces/swarms/questionnaire-swarm-interface";
import { inject, injectable } from "inversify";
import { Observable } from "rxjs";
import { IQuestionnaire, IQuestion } from "../entities/adapter/questionnaire-entities";

import { ILogger } from "../logger/logger-interface";
@injectable()
export class QuestionnaireSwarm implements IQuestionnaireSwarm{

    constructor(
        @inject("QuestionnaireAdapter") private questionnaireAdapter: IQuestionnaireAdapter,
        @inject("Logger") private logger: ILogger) {}

    loadQuestionnaires(): Observable<IQuestionnaire[]> {
        this.logger.debug("QuestionnaireSwarm:loadQuestionnaires");
        return this.questionnaireAdapter.getQuestionnaires();
    }

    loadQuestionnaire(id: string): Observable<IQuestionnaire> {
        this.logger.debug("QuestionnaireSwarm:loadQuestionnaire:",id);
        return this.questionnaireAdapter.getQuestionnaire(id);
    }

    loadQuestions(id: string):Observable<IQuestion[]> {
        this.logger.debug("QuestionnaireSwarm:loadQuestions:", id);
        return this.questionnaireAdapter.getQuestions(id);
    }

    loadQuestion(id: string):Observable<IQuestion> {
        this.logger.debug("QuestionnaireSwarm:loadQuestions:", id);
        return this.questionnaireAdapter.getQuestion(id);
    }
}

