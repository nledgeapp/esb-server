import { inject, injectable } from "inversify";
import { IAttributeAdapter } from "../interfaces/adapters/attribute-adapter-interface";
import { IAttributeSwarm } from "../interfaces/swarms/attribute-swarm-interface";
import { Observable } from "rxjs";
import { IAttribute } from "../entities/adapter/attribute-entities";

import { ILogger } from "../logger/logger-interface";
@injectable()
export class AttributeSwarm implements IAttributeSwarm{

    constructor(
        @inject("AttributeAdapter") private attributeAdapter: IAttributeAdapter,
        @inject("Logger") private logger: ILogger) {}

    getAttributes(params):Observable<IAttribute[]>{
        this.logger.debug("AttributeSwarm:getAttributes:",params);
        return this.attributeAdapter.getAttributes(params);
    }

    postAttribute(attribute:IAttribute):Observable<IAttribute>{
        this.logger.debug("AttributeSwarm:postAttribute:",attribute);
        return this.attributeAdapter.postAttribute(attribute);
    }

}