import * as express from 'express';
import { QuestionnaireSwarm } from './swarms/questionnaire-swarm';
import kernel from "./di/setup-di";
import { IQuestionnaireEndpoint } from "./interfaces/endpoints/questionnaire-endpoint-interface";
import { IAttributeEndpoint } from "./interfaces/endpoints/attribute-endpoint-interface";
import { IAuthentication } from "./interfaces/endpoints/authentication-interface";
import * as log from 'bog';
export function setupRoutes(app) {
    setupRoot(app);
    setupAuthorization(app);
    setupQuestionnaireRoutes(app);
    setupAttributeRoutes(app);
}

function setupRoot(app: express.Application){
    app.get("/", (request, response) => response.sendStatus(200));
}

function setupAuthorization(app: express.Application){
    var authentication:IAuthentication = kernel.get<IAuthentication>("Authentication");
    app.all("*", (request, response, next) => authentication.required(request, response, next));
}

function setupQuestionnaireRoutes(app: express.Application){
    var questionnareEndpoint:IQuestionnaireEndpoint = kernel.get<IQuestionnaireEndpoint>("QuestionnaireEndpoint");
    app.get('/questionnaire', (request, response) => questionnareEndpoint.getQuestionnaires(request,response));
    app.get('/questionnaire/:id', (request, response) => questionnareEndpoint.getQuestionnaire(request,response)); 
    app.get('/questionnaire/:id/questions', (request, response) => questionnareEndpoint.getQuestions(request,response));
    app.get('/question/:id', (request, response) => questionnareEndpoint.getQuestions(request,response));    
    log.debug('setup questionnaire-routes');
};

function setupAttributeRoutes(app: express.Application){
    var attributeEndpoint:IAttributeEndpoint = kernel.get<IAttributeEndpoint>("AttributeEndpoint");
    app.get('/attributes', (request, response) => attributeEndpoint.getAttributes(request,response));
    app.post('/attributes', (request, response) => attributeEndpoint.postAttributes(request,response));
    log.debug('setup questionnaire-routes');
};

