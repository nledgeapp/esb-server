export interface IAttribute{
    name: String,
    type: String,
    source: [String],
    group: String
}