export interface IQuestionnaire{
    _id:string;
    title: string;
    description: string;
}

export interface IQuestion{
    _id:string;
    question: string;
    questionnaire_id: string;
    alternatives:any[];
}