export interface IUserAuthenticationCookies{
    linkedinToken: string,
    slackId: string,
    slackToken: string
}

export interface ILinkedInProfile {    
    currentShare: {
        author: {
            firstName: string,
            id: string,
            lastName: string
        },
        comment: string,
        content: {
            description: string,
            eyebrowUrl: string,
            resolvedUrl: string,
            shortenedUrl: string,
            submittedImageUrl: string,
            submittedUrl: string,
            thumbnailUrl: string,
            title: string
        },
        id: string,
        source: {
            serviceProvider: {
                name: string
            }
        },
        timestamp: number,
        visibility: {
            code: string
        }
    },
    emailAddress: string,
    firstName: string,
    formattedName: string,
    headline: string,
    id: string,
    industry: string,
    lastName: string,
    location: {
        country: {
            code: string
        },
        name: string
    },
    numConnections: number,
    numConnectionsCapped: boolean,
    pictureUrl: string,
    pictureUrls: {
        _total: number,
        values: [
            string
        ]
    },
    positions: {
        _total: number,
        values: [
            {
                company: {
                    id: number,
                    industry: string,
                    name: string,
                    size: string,
                    type: string
                },
                id: number,
                isCurrent: boolean,
                location: {
                    country: {
                        code: string,
                        name: string
                    },
                    name: string
                },
                startDate: {
                    month: number,
                    year: number
                },
                summary: string,
                title: string
                },
                {
                company: {
                    id: number,
                    industry: string,
                    name: string,
                    size: string,
                    type: string
                },
                id: number,
                isCurrent: boolean,
                location: {
                    name: string
                },
                startDate: {
                    year: number
                },
                summary: string,
                title: string
            }
        ]
    },
    publicProfileUrl: string
}

export interface ISlackProfile{
    status: number,
    data: {
        user: {
            id: string,
            team_id: string,
            name: string,
            deleted: boolean,
            status: string,
            color: string,
            real_name: string,
            tz: string,
            tz_label: string,
            tz_offset: number,
            profile: {
                first_name: string,
                last_name: string,
                title: string,
                skype: string,
                phone: string,
                image_number: string,
                image_original: string,
                avatar_hash: string,
                real_name: string,
                real_name_normalized: string,
                email: string
            },
            is_admin: boolean,
            is_owner: boolean,
            is_primary_owner: boolean,
            is_restricted: boolean,
            is_ultra_restricted: boolean,
            is_bot: boolean,
            has_numberfa: boolean
        }
    }
}

export interface SlackUserInfo {
    status: number,
    data: {
        presence: string,
        online: boolean,
        auto_away: boolean,
        manual_away: boolean,
        connection_count: number
    }
}

export interface SlackChannels{
    status: number,
    data: [
        {
            name: string,
            id: string,
            topic: {
                value: string,
                creator: string,
                last_set: number
            },
            purpose: {
            value: string,
            creator: string,
            last_set: number
        }
        },
        {
            name: string,
            id: string,
            topic: {
                value: string,
                creator: string,
                last_set: number
            },
            purpose: {
                value: string,
                creator: string,
                last_set: number
            }
        }
    ]
}

export interface SlackStars{
    status: number,
    data: {
        items: [
            {
                type: string,
                channel: string,
                message: {
                    type: string,
                    user: string,
                    text: string,
                    ts: string,
                    permalink: string,
                    is_starred: boolean
                }
            }
        ],
        paging: {
            count: number,
            total: number,
            page: number,
            pages: number
        }
    }
}