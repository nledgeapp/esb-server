import * as express from 'express';
import * as compression from 'compression';
    //methodOverride = require('method-override'),
import * as  bodyParser from 'body-parser';
import * as cookieParser from'cookie-parser';

export function setupApp(app) {
    var env = app.get('env');
    app.use(compression());
    app.use(cookieParser());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
    //app.use(methodOverride());
};
