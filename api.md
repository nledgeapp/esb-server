# Authorization

use authorization endpoints at the following links and put the received tokens in your browsers cookies
1) https://edge-summit-user-service.herokuapp.com/api/login/linkedin
put the received token to the cookies with this key -> linkedToken
f.e. document.cookies="linkedinToken=%accessToken%"
2) https://edge-summit-user-service.herokuapp.com/api/login/slack
put the received token and id to the cookies with these keys -> slackId,slackToken
f.e. document.cookies="slackToken=%accessToken%"
and document.cookies="slackId=%id%"


# Question API

GET /questionnaire
Response:
```
	[
		{
			_id: String,
			title: String
			description: String
			...
		}
	]
```

GET /questionnaire/:id
Response:
```
	{
		_id: String,
		title: String
		description: String
		...
	}
```

GET /questionnaire/:id/questions
Response:
```
	[
		{
			id: String
			question: String
			min_choises: Int
			max_choises: Int
			alternatives: [
				{
					attribute_id: String
				}
			]
		}
	]
```

GET /question/:id
Response:
```
	{
		id: String
		question: String
		min_choises: Int
		max_choises: Int
		alternatives: [
			{
				attribute_id: String
			}
		]
	}
```

# Attribute API

GET /attribute?id=xxx&name=xxx&type=xxx&source=xxx&group=xxx

Response:
```
	[
		{
			_id: String,
			name: String,
			type: String,
			group: String,
			normalized: {
				name: String,
				type: String,
				sources: [String],
				group: String
			},
			__v: Int,
			sources: [String]
		}
	]
```

POST /attribute 

Parameters
```
	{
		name: String,
		type: String,
		source: [String],
		group: String
	}
```